
# NPM Packaging With Version Management

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/language-specific/npm/npm-packaging)


## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-10-29

* **GitLab Version Released On**: 13.5

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **Tested On**: 
  * GitLab Docker-Executor Runner

* **References and Featured In**:

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **Development AntiPattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** This approach to version storage can result in duplicate version numbered packages attempting to be push to the same NPM package endpoint.  Please see this example for storing the version in CI/CD Variable to fix this antipattern: https://docs.gitlab.com/ee/user/packages/generic_packages/.  Note, if there are not multiple branches under development that also push to the same NPM package endpoint, you would not be affected by this antipattern.
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Build and upload an NPM package to the project's own NPM registry.

### Cross References and Documentation
* **Cross Reference Pattern:** * Example of using CI CD varaibles to store the version number: https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline
* NPM Packages Feature Help: https://docs.gitlab.com/ee/user/packages/npm_registry/

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.
